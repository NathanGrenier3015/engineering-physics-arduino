// Coins are 110cm from top, 70cm from left

#include <Servo.h>
Servo leftservo;  
Servo rightservo;  
const int pingPin = 5; // Trigger Pin of Ultrasonic Sensor
const int echoPin = 6; // Echo Pin of Ultrasonic Sensor
long coinDistance1 = 110; // In cm
long coinDistance2 = 70; // In cm
long coinDistance = coinDistance1;
long difference;
long duration;
long distance;
long gap = 350; // in cm
int correctionTime;
long dp = 22.7; // delta proportional constant (to be calibrated)
bool turn1 = false;

void setup() {
  leftservo.attach(9);  
  rightservo.attach(10);
   //set up the Serial
  Serial.begin(9600);
  //setupt the pin modes  
  pinMode(pingPin, OUTPUT);
  pinMode(echoPin, INPUT);
  leftservo.write(90);
  rightservo.write(90);
  
  delay(5);


  /*
    Move forward:
    left = -
    right = +
    
    Move Backwards:
    left = +
    right = -

    Turn Left:
    left = +
    right = +

    Turn Right:
    left = -
    right = -
  */

  /*  Calibrating dp
  Serial.println(getDistance());
  rotate90Deg(1);
  forward(1000);
  rotate90Deg(0);
  Serial.println(getDistance());
  */
}

void loop(){
  Serial.println(setSpeed(100));
}


int invertSpeed(int speed){
  return 180 - speed;
}

int setSpeed(int speed_in) {
  // Takes an input from 0% to 100% and maps it to a value servo.write can read
  return map(speed_in, 0, 100, 90, 0);
}

/*    Speeds for servo.writeMicroseconds()
int invertSpeed(int speed){
  return 3000 - speed;
}

int setSpeed(int speed_in) {
  return map(speed_in, 0, 100, 1500, 2000);
}
*/


int turnSetSpeed(char direction, int speed){
  speed = setSpeed(speed);

  if (direction == "left"){
    leftservo.write(); // +
    rightservo.write(); // +
  }

  if (direction == "right"){

  }
}

long getDistance() {
  digitalWrite(pingPin, LOW);
  delayMicroseconds(2);
  //send the 10 microsecond trigger
  digitalWrite(pingPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(pingPin, LOW);
  //get the pulse duration in microseconds
  duration = pulseIn(echoPin, HIGH);
  return distance = (duration * 0.034) / 2;
}

void stop() {
  leftservo.write(90);
  rightservo.write(90);
}

void forward(int time) {
  leftservo.write(180);
  rightservo.write(0);
  delay(time);
}

void rotate90Deg(int direction) {
  // Rotate CCW
  if (direction == 1) {
    leftservo.write(10);
    rightservo.write(10);
    delay(1670);
    stop();
  }
  // Rotate CW
  if (direction == 0) {
    leftservo.write(170);
    rightservo.write(170);
    delay(1670);
    stop();
  }
}

/*
void loop() {
  // Find out distance to be corrected for
  difference = getDistance() - coinDistance;
  correctionTime = abs(difference * dp);
  
  Serial.print("Distance: ");
  Serial.println(distance);

  Serial.print("Difference: ");
  Serial.println(difference);
  

  // Check for gap in wall and turn
  if (getDistance() > gap && turn1 == false){
    rotate90Deg(0);
    coinDistance = coinDistance2;
    turn1 = true;
    // If not set to zero, code below will run and turn again
    difference = 0;
  }

  // rotate bot
  if (difference > 5) {
    // rotate twords wall
    rotate90Deg(1);
    // correct distance
    forward(correctionTime);
    rotate90Deg(0);
  } else if (difference < -5) {
    //rotate away from wall
    rotate90Deg(0);
    //correct distance
    forward(correctionTime);
    rotate90Deg(1);
  } else {
    forward(1000);
  }
}
*/