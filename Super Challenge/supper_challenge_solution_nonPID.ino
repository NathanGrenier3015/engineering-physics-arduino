// Coins are 110cm from top, 70cm from left

#include <Servo.h>


// Set up Servo
Servo leftservo;  
Servo rightservo;  
const int pingPin = 5; // Trigger Pin of Ultrasonic Sensor
const int echoPin = 6; // Echo Pin of Ultrasonic Sensor

// Define known values and flags
long coinDistance1 = 110; // In cm
long coinDistance2 = 70; // In cm
long coinDistance = coinDistance1;
long gap = 350; // in cm
bool turn1 = false;
int tolerance1 = 8; //in cm
int tolerance2 = 10; //in cm
int tolerance = tolerance1;
int forwardInterval = 1000; // in milliseconds

// Variables to be calculated or set
long difference;
long duration;
long distance;
int correctionTime;

// PID Coefficents
long dp = 22.7; // delta proportional constant (to be calibrated)


void setup() {
  leftservo.attach(9);  
  rightservo.attach(10);
   //set up the Serial
  Serial.begin(9600);
  //setupt the pin modes  
  pinMode(pingPin, OUTPUT);
  pinMode(echoPin, INPUT);
  leftservo.write(90);
  rightservo.write(90);
  
  /*  Calibrating dp
  Serial.println(getDistance());
  rotate90Deg(1);
  forward(1000);
  rotate90Deg(0);
  Serial.println(getDistance());
  */
}

long getDistance() {
  digitalWrite(pingPin, LOW);
  delayMicroseconds(2);
  //send the 10 microsecond trigger
  digitalWrite(pingPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(pingPin, LOW);
  //get the pulse duration in microseconds
  duration = pulseIn(echoPin, HIGH);
  return distance = (duration * 0.034) / 2;
}

void stop() {
  leftservo.write(90);
  rightservo.write(90);
}

void forward(int time) {
  leftservo.write(180);
  rightservo.write(0);
  delay(time);
}

void rotate90Deg(int direction) {
  // Rotate CCW
  if (direction == 1) {
    leftservo.write(10);
    rightservo.write(10);
    delay(1670);
    stop();
  }
  // Rotate CW
  if (direction == 0) {
    leftservo.write(170);
    rightservo.write(170);
    delay(1670);
    stop();
  }
}

void loop() {
  // Find out distance to be corrected for
  difference = getDistance() - coinDistance;
  correctionTime = abs(difference * dp);
  
  Serial.print("Distance: ");
  Serial.println(distance);

  Serial.print("Difference: ");
  Serial.println(difference);
  

  // Check for gap in wall and turn
  if (getDistance() > gap && turn1 == false){
    rotate90Deg(0);
    coinDistance = coinDistance2;
    turn1 = true;
    tolerance = tolerance2; // The coins on the other wall seem to require a greater tolerance as the robot swerves more
    
    difference = 0;   // If not set to zero, code below will run and turn again
  }

  // rotate bot
  if (difference > tolerance) {
    // rotate twords wall
    rotate90Deg(1);
    // correct distance
    forward(correctionTime);
    rotate90Deg(0);
  } else if (difference < -tolerance) {
    //rotate away from wall
    rotate90Deg(0);
    //correct distance
    forward(correctionTime);
    rotate90Deg(1);
  } else {
    forward(forwardInterval);
  }
}
